module.exports = {
    publicPath: process.env.NODE_ENV === 'production' ? 'https://ryan-shang.gitlab.io/gitlab-workflow-demo' : '',
    pages: {
        index: {
            entry: 'src/main.js',
            template: 'static/index.html',
            title: 'gitlab-workflow-demo',
            faviconPath: 'static/favicon.ico'
        }
    }
}